//Soal 1
var nilai = 75;
var index;

if ( nilai >= 85 ){
    index = 'A'
} else if ( nilai >= 75 && nilai < 85 ){
    index ='B'
} else if ( nilai >= 65 && nilai < 75 ){
    index ='C'
} else if ( nilai >= 55 && nilai < 65 ){
    index ='D'
} else {
    index ='E'
}

console.log(index)

//Soal 2
var tanggal = 28;
var bulan   = 5;
var tahun   = 1990;
var bulanStr

switch(bulan) {
    case 1: 
        bulanStr = 'Januari'
        break;
    case 2: 
        bulanStr = 'Februari'
        break;
    case 3: 
        bulanStr = 'Maret'
        break;
    case 4: 
        bulanStr = 'April'
        break;
    case 5: 
        bulanStr = 'Mei'
        break;
    case 6: 
        bulanStr = 'Juni'
        break;
    case 7: 
        bulanStr = 'Juli'
        break;
    case 8: 
        bulanStr = 'Agustus'
        break;
    case 9: 
        bulanStr = 'September'
        break;
    case 10: 
        bulanStr = 'Oktober'
        break;
    case 11: 
        bulanStr = 'November'
        break;
    case 12: 
        bulanStr = 'Desember'
        break;
    default:
        bulanStr = 'bulan error'
        break;
}

var str = tanggal + ' ' + bulanStr + ' ' + tahun
console.log(str)

//Soal 3
var n
n = 7

for ( var baris = 1; baris <= n; baris++ ){
    var pagar = ''    
    for ( var kolom = 1; kolom <= baris; kolom++ ){
        pagar = pagar + '#'
    }
    console.log(pagar)
}

//soal 4
var m;
m=3

for(var i=1; i<=m; i++){
    var hasilMod3 = i%3

    if(hasilMod3 == 1){
        str4 = i + ' = I love programming'
    }else if(hasilMod3 == 2){
        str4 = i + ' = I love javascript'
    } else{
        var samaDengan = ''
        for(var j=1; j<=i; j++){
            samaDengan = samaDengan + '='
        }
        str4 = i + ' = I love vue js' + '\n' + samaDengan
    }
    console.log(str4)
}