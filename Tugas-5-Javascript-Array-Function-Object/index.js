//Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()

for(i=0; i<daftarHewan.length; i++){
    console.log( daftarHewan[i] )
}

//Soal 2
function introduce(data) {
    return "Nama saya "+ data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + " dan saya punya hobby yaitu " + data.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) 

//Soal 3
function hitung_huruf_vokal(Muhammad) {
	var vowelsCount = 0;
	let vowels = 'aeiou';

	for (var i = 0; i < Muhammad.length; i++) {
		if (vowels.indexOf(Muhammad[i]) != -1) {
			vowelsCount++;
		}
	}
	return vowelsCount;
}

function hitung_huruf_vokal(Iqbal) {
	var vowelsCount = 0;
	let vowels = 'aeiou';

	for (var i = 0; i < Iqbal.length; i++) {
		if (vowels.indexOf(Iqbal[i]) != -1) {
			vowelsCount++;
		}
	}
	return vowelsCount;
}

var Muhammad = "muhammad"
var Iqbal = "iqbal"

var hitung_1 = hitung_huruf_vokal(Muhammad)
var hitung_2 = hitung_huruf_vokal(Iqbal)
console.log(hitung_1, hitung_2);