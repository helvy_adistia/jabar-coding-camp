//Kuis 1
//1. Function Penghitung Jumlah Kata

function jumlah_kata1() {
    var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok"
    var totalKata = 0;
  
    for (var i = 0; i < kalimat_1.length; i++)
        if (kalimat_1[i] === " ") { 
            totalKata = totalKata+1;
    }
    totalKata = totalKata+ 1; 
    return totalKata
}

console.log(jumlah_kata1());

function jumlah_kata2() {
    var kalimat_2 = "Saya Iqbal"
    var totalKata = 0;
  
    for (var i = 0; i < kalimat_2.length; i++)
        if (kalimat_2[i] === " ") { 
            totalKata = totalKata+1;
    }
    totalKata = totalKata+ 1; 
    return totalKata
}

console.log(jumlah_kata2());

//2. Function Penghasil Tanggal Hari Esok

//Contoh 1
var tanggal = 29
var bulan = 2
var tahun = 2020

function next_date() {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    const tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}
next_date(tanggal, bulan, tahun);

//Contoh 2
var tanggal = 28
var bulan = 2
var tahun = 2021

function next_date() {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    const tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}
next_date(tanggal, bulan, tahun);

//Contoh 3
var tanggal = 31
var bulan = 12
var tahun = 2020

function next_date() {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    const tglBaru = new Date(tahun, bulan-1, tanggal+1);
    console.log(tglBaru.toLocaleDateString("id", format));
}
next_date(tanggal, bulan, tahun);